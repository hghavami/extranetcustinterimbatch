
package com.usatoday.batch.custinterim;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.customer.CustomerInterimInfoIntf;
import com.usatoday.businessObjects.customer.CustomerInterimInfoBO;
import com.usatoday.businessObjects.util.ZeroFilledNumeric;
import com.usatoday.businessObjects.util.mail.EmailAlert;
import com.usatoday.integration.CustomerInterimInfoDAO;

/** 
 * 
 * @author hghavami
 * @date November, 2006
 * @class CustInterimInfoBatch
 * 
 * This class processes Customer Interim Info data to an FTP server for pick up by the Viper server.
 * The general flow is:
 * 		- Connect to Esub DB on extranet
 * 		- Change the state of records that are ready for batch to the IN BATCH PROCESSING State.
 * 		- Write the records to a flat file in this format:
 * 			 
 *           ID             bigint      8   0
 *           PubCode        char        2   1
 *           Email          char        60  1
 *           FirstName      char    25  1
 *           LastName       char    25  1
 *           FirmName       char    28  1
 *           Address1       char    28  1
 *           Address2       char    28  1
 *           City           char        28  1
 *           State          char        2   1
 *           Zip            char        5   1
 *           HomePhone      char    10  1
 *           WorkPhone      char    10  1
 *           BillFirstName  char    10  1
 *           BillLastName   char    15  1
 *           BillFirmName   char    28  1
 *           BillAddress1   char    28  1
 *           BillAddress2   char    28  1
 *           BillCity       char    28  1
 *           BillState      char    2   1
 *           BillZip        char    5   1
 *           BillPhone      char    10  1
 *           RateCode       char    2   1
 *           PromoCode      char    2   1
 *           ContestCode    char    2   1
 *           SrcOrdCode     char    1   1
 *           SubsAmount     char    7   1
 *           SubsDur        char    3   1
 *           StartDate      char    10  1
 *           Insert_timestamp datetime    8   0
 *           ProcessState   int 4   0
 * 	  
 *		- Encrypt the flat file using GNUPG
 *		- FTP the file to the ftp server
 *		- Update the status of the records to READY FOR DELETION stat
 *		- Delete any records older than threshold
 *
 */
public class CustInterimInfoBatch {
    
    String propertyFileName = null;
    
    String jndiName = "java:comp/env/esub";
    int numberOfDaysBeforePurge = 2;
    String logFileDirName = "C:\\USAT\\processed\\logs\\";
    String dataOutputDirName = "C:\\USAT\\processed\\archive\\";
    String baseOuputFileName = "XTRNTINTCSTINF_";
    String gpgHomeDir = "c:\\gnupg";
    String gpgKeyAlias = "aeast@usatoday.com";
    String FTPHost = "159.54.128.31";
    String ftpUserId = "extftptest";
    String ftpPwd = "19extftptest99";
    boolean deletePlainTextFile = true;
    boolean deleteArchiveFile = false;
    boolean sendReport = false;
    String reportRecipients = null;
    String alertRecipients = "hghavami@usatoda.com";
    
    
    // flag that allows the doWork method to execute only one time per instance
    boolean workDone = false;
    
    Calendar startTime = null;
    Calendar endTime = null;
    
    int recordsProcessed = 0;
    
    ArrayList<String> alertMessages = new ArrayList<String>();
    ArrayList<String> processingMessages = new ArrayList<String>();
    
    HashSet<String> ordersHash = new HashSet<String>();
    
    Logger processLog = null;
    Level  logLevel = Level.WARNING;
    
    /**
     * @return Returns the alertMessages.
     */
    public ArrayList<String> getAlertMessages() {
        return this.alertMessages;
    }
    /**
     * @return Returns the endTime.
     */
    public Calendar getEndTime() {
        return this.endTime;
    }
    /**
     * @return Returns the ordersHash.
     */
    public HashSet<String> getOrdersHash() {
        return this.ordersHash;
    }
    /**
     * @return Returns the processLog.
     */
    public Logger getProcessLog() {
        return this.processLog;
    }
    /**
     * @return Returns the processingMessages.
     */
    public ArrayList<String> getProcessingMessages() {
        return this.processingMessages;
    }
    /**
     * @return Returns the startTime.
     */
    public Calendar getStartTime() {
        return this.startTime;
    }
    /**
     * @return Returns the gpgKeyAlias.
     */
    public String getGpgKeyAlias() {
        return this.gpgKeyAlias;
    }
    /**
     * @param gpgKeyAlias The gpgKeyAlias to set.
     */
    public void setGpgKeyAlias(String gpgKeyAlias) {
        this.gpgKeyAlias = gpgKeyAlias;
    }

    /**
     * @return Returns the alertRecipients.
     */
    public String getAlertRecipients() {
        return this.alertRecipients;
    }
    /**
     * @param alertRecipients The alertRecipients to set.
     */
    public void setAlertRecipients(String alertRecipients) {
        this.alertRecipients = alertRecipients;
    }
    /**
     * @return Returns the deleteArchiveFile.
     */
    public boolean isDeleteArchiveFile() {
        return this.deleteArchiveFile;
    }
    /**
     * @param deleteArchiveFile The deleteArchiveFile to set.
     */
    public void setDeleteArchiveFile(boolean deleteArchiveFile) {
        this.deleteArchiveFile = deleteArchiveFile;
    }
    /**
     * @return Returns the reportRecipients.
     */
    public String getReportRecipients() {
        return this.reportRecipients;
    }
    /**
     * @param reportRecipients The reportRecipients to set.
     */
    public void setReportRecipients(String reportRecipients) {
        this.reportRecipients = reportRecipients;
    }
    /**
     * @return Returns the sendReport.
     */
    public boolean isSendReport() {
        return this.sendReport;
    }
    /**
     * @param sendReport The sendReport to set.
     */
    public void setSendReport(boolean sendReport) {
        this.sendReport = sendReport;
    }
    /**
     * @return Returns the deletePlainTextFile.
     */
    public boolean isDeletePlainTextFile() {
        return this.deletePlainTextFile;
    }
    /**
     * @param deletePlainTextFile The deletePlainTextFile to set.
     */
    public void setDeletePlainTextFile(boolean deletePlainTextFile) {
        this.deletePlainTextFile = deletePlainTextFile;
    }
    /**
     * @return Returns the recordsProcessed.
     */
    public int getRecordsProcessed() {
        return this.recordsProcessed;
    }
    /**
     * @param recordsProcessed The recordsProcessed to set.
     */
    public void setRecordsProcessed(int recordsProcessed) {
        this.recordsProcessed = recordsProcessed;
    }
    /**
     * @return Returns the baseOuputFileName.
     */
    public String getBaseOuputFileName() {
        return this.baseOuputFileName;
    }
    /**
     * @param baseOuputFileName The baseOuputFileName to set.
     */
    public void setBaseOuputFileName(String baseOuputFileName) {
        this.baseOuputFileName = baseOuputFileName;
    }
    /**
     * @return Returns the dataOutputDirName.
     */
    public String getDataOutputDirName() {
        return this.dataOutputDirName;
    }
    /**
     * @param dataOutputDirName The dataOutputDirName to set.
     */
    public void setDataOutputDirName(String dataOutputDirName) {
        this.dataOutputDirName = dataOutputDirName;
    }
    /**
     * @return Returns the gpgHomeDir.
     */
    public String getGpgHomeDir() {
        return this.gpgHomeDir;
    }
    /**
     * @param gpgHomeDir The gpgHomeDir to set.
     */
    public void setGpgHomeDir(String gpgHomeDir) {
        this.gpgHomeDir = gpgHomeDir;
    }
    /**
     * @return Returns the jndiName.
     */
    public String getJndiName() {
        return this.jndiName;
    }
    /**
     * @param jndiName The jndiName to set.
     */
    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }
    /**
     * @return Returns the logFileDirName.
     */
    public String getLogFileDirName() {
        return this.logFileDirName;
    }
    /**
     * @param logFileDirName The logFileDirName to set.
     */
    public void setLogFileDirName(String logFileDirName) {
        this.logFileDirName = logFileDirName;
    }
    /**
     * @return Returns the numberOfDaysBeforePurge.
     */
    public int getNumberOfDaysBeforePurge() {
        return this.numberOfDaysBeforePurge;
    }
    /**
     * @param numberOfDaysBeforePurge The numberOfDaysBeforePurge to set.
     */
    public void setNumberOfDaysBeforePurge(int numberOfDaysBeforePurge) {
        this.numberOfDaysBeforePurge = numberOfDaysBeforePurge;
    }
    
    /**
     * The launch point one parameter is the location of the property file
     * @param args
     */
	public static void main(String[] args) {
		
	    CustInterimInfoBatch batch = new CustInterimInfoBatch();
	    
	    if ( (args.length == 1 && args[0].indexOf("help")>=0) || args.length > 1) {
	        System.out.println("Useage: CustInterimInfoBatch [propertyFile], otherwise default values will be used");
	        System.exit(0);
	    }
	    
	    if (args.length == 1 && args[0].indexOf("help")< 0) {
	        // set the property file
	        try {
	            batch.setPropertyFileName(args[0]);
	        }
	        catch (Exception e) {
	            System.out.println("Error: " + e.getMessage());
	            System.exit(1);
	        }
	    }
	    else if (args.length > 1) {
	        // invalid startup arguments
	        System.out.println("Terminating. Useage: CustInterimInfoBatch [propertyFile], otherwise default values will be used");
	        System.exit(1);
	    }
	    
	    batch.initializeLogging(args);
	    
	    batch.doWork();
	}

	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public CustInterimInfoBatch() {
		super();
	}

	/**
	 * this method walks through the logic of the application
	 * it does all the work
	 */
	private void doWork() {
	    this.getProcessLog().entering(this.getClass().getName(), "doWork");
	    
	    boolean sendAlert = false;
	    
	    if (this.workDone) {
		    this.getProcessLog().warning("Call to doWork more than one time. Doing nothing.");
	        return;
	    }
	    else {
	        this.workDone = true;
	    }
	    
	    this.startTime = Calendar.getInstance();
	    
	    int rowsAffected = 0;
	    // change state of customer interim information to in batch processing
	    try {
	        rowsAffected = CustomerInterimInfoBO.changeStateToInBatchProcessing();
	        this.getProcessLog().fine("Number of Customer Interim Info Records set to BATCH PROCESSING state: " + rowsAffected);
	    }
	    catch (Exception e) {
	        sendAlert = true;
            alertMessages.add("Exception changing state of records to BATCH_PROCESSING: " + e.getMessage());
            this.getProcessLog().severe("Exception setting records' state to BATCH PROCESSING state: " + e.getMessage());
        }

	    Collection<CustomerInterimInfoIntf> attributes = null;
        CustomerInterimInfoDAO dao = new CustomerInterimInfoDAO();
	    // retrieve the customer interim record attributes
	    try {
	        attributes = dao.fetchAttributesRecordsForBatchProcessing();
	        
	        if (!(attributes != null && attributes.size() == rowsAffected)) {
	            alertMessages.add("Number set for batch processing and number pulled for processing don't match. Possible failure of a previous execution.");
	            sendAlert = true;
	            this.getProcessLog().warning("Numbers or records that had state changed (" + rowsAffected + ") doesn't match # returned for processing (" + attributes.size() + "). Possible failure in previous execution.");
	        }
	    }
	    catch (Exception e) {
	        alertMessages.add("Exception fetching attribute records: " + e.getMessage());
	        sendAlert = true;
	        this.getProcessLog().severe("Exception fetching attribute records: " + e.getMessage());
        }

	    java.io.File dataFile = null;
	    String time = null;
	    // process the records to the file
	    try {
	        Calendar cal = Calendar.getInstance();
	        time = ZeroFilledNumeric.getValue(4, cal.get(Calendar.YEAR)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.MONTH)+1) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.DAY_OF_MONTH)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.HOUR_OF_DAY)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.MINUTE)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.SECOND));
	        String pathSepStr = File.separator;
	        if (!this.getDataOutputDirName().endsWith(pathSepStr)) {
	            this.setDataOutputDirName(this.getDataOutputDirName() + pathSepStr);
	        }
	        java.io.File f = new File(this.getDataOutputDirName()+this.getBaseOuputFileName()+ time);

	        this.getProcessLog().fine("Created temporary data file: " + f.getAbsolutePath());

	        // logic to create a unique file
	        while (f.exists()) {
		        cal = Calendar.getInstance();
		        time = ZeroFilledNumeric.getValue(4, cal.get(Calendar.YEAR)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.MONTH)+1) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.DAY_OF_MONTH)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.HOUR_OF_DAY)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.MINUTE)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.SECOND));  
		        f = new File(this.getDataOutputDirName()+this.getBaseOuputFileName()+ time);
		        this.getProcessLog().fine("File already existed. Created new temporary data file: " + f.getAbsolutePath());
	        }
	        
	        dataFile = f;
	        
	        PrintWriter oFile = new PrintWriter(new java.io.FileOutputStream(f));
	        
	        Iterator<CustomerInterimInfoIntf> iter = attributes.iterator();
	        while (iter.hasNext()) {
	            CustomerInterimInfoBO element = new CustomerInterimInfoBO(iter.next());
	            
	            try {
	                String formattedOutputString = element.getBatchFormattedString();
	                oFile.print(formattedOutputString);
	                oFile.println();
	                recordsProcessed++;
	                // add the order id to the has set for a count of orders
	                // processed versus attribute records processed.
	                this.getOrdersHash().add(String.valueOf(element.getOrderID()));
	                this.getProcessLog().fine("Processed Record with Key: " + element.getOrderID());
	                this.getProcessLog().finest("Formatted String: " + formattedOutputString);	                
	            }
	            catch (UsatException ue) {
	                sendAlert = true;
	                String errMsg = "Failed to process attribute with key = " + element.getOrderID() + " Error: " + ue.getMessage();
	            	alertMessages.add(errMsg);
	            	this.getProcessLog().severe(errMsg);
	            	try {
	            	    // change state to ERROR_SENDING BATCH
	            	    this.getProcessLog().severe("Attempting to set state of record to BATCH_ERROR state");
	            	    rowsAffected = CustomerInterimInfoBO.changeStateToBatchError();
	            	    this.getProcessLog().severe("Record set to error state.");
	            	}
	            	catch (Exception e) {
	            	    this.getProcessLog().severe("Failed to update state of attribute to BATCH_ERROR: Key=" + element.getOrderID());
		            	alertMessages.add("Failed to update state to BATCH_ERROR = " + element.getOrderID() + " Error: " + e.getMessage());
	            	}
                }
            }
	        
	        oFile.close();
	        
	        if (recordsProcessed == 0) {
	            f.delete();
	            this.getProcessLog().warning("No records processed..deleted temporary data file.");
	        }
	    }
	    catch (Exception e) {
            alertMessages.add("Unexpected Exception processing records: " + e.getMessage());
            sendAlert = true;
	        this.getProcessLog().severe("Unexpected Exception processing records: " + e.getMessage());
        }
	    // Text file name
        File ftpNamedFile = dataFile;
//        ftpNamedFile.renameTo(new File(dataFile.getAbsolutePath()+".txt"));
        
/*	    File encryptedFile = new File(dataFile.getAbsolutePath()+".gpg");
	    //File ftpNamedFile = new File(this.getDataOutputDirName() + "XTRNTINTCSTINF_.gpg");
	    
	    // encrypt the file
	    try {
	        if (encryptedFile.exists()) {
	            encryptedFile.renameTo(new File(this.getDataOutputDirName()+ "CUSTINTERIM_BACKUP.bkp" + time));
	            encryptedFile = new File(dataFile.getAbsolutePath()+".gpg");
	        }
	        
	        if (this.getRecordsProcessed()>0) {
		        Runtime rt = Runtime.getRuntime();
		        this.getProcessLog().fine("Forking process: '" + "gpg --homedir "+ this.getGpgHomeDir()+ " --output " + encryptedFile.getAbsolutePath() + " --encrypt --recipient " + this.getGpgKeyAlias() +  " " + dataFile.getAbsolutePath() + "'");
		       // Process p = rt.exec("gpg --homedir "+ this.getGpgHomeDir()+ " --output " + encryptedFile.getAbsolutePath() + " --encrypt --recipient " + this.getGpgKeyAlias() +  " " + dataFile.getAbsolutePath());
		        Process p = rt.exec("gpg --homedir "+ this.getGpgHomeDir()+ " --output " + encryptedFile.getAbsolutePath() + " --encrypt --recipient " + this.getGpgKeyAlias() +  " " + dataFile.getAbsolutePath());
		        
		        int exitCode = p.waitFor();
		        this.getProcessLog().fine("Encryption process finished with exit code: " + exitCode);
		        if (exitCode != 0){
		            // failed to run process
		            throw new UsatException("Failed to encrypt file, gpg process ended with exit code: " + exitCode);
		        }
		        else {
		            if (this.isDeletePlainTextFile()) {
		                dataFile.delete();
		                this.getProcessLog().fine("Deleted temporary data file.");		                
		            }
		        }
	        }
	    }
	    catch (Exception e) {
	        sendAlert = true;
            alertMessages.add("Failed to encrypt Customer Interim Info data file: " + e.getMessage());
            this.getProcessLog().severe("Failed to encrypt Customer Interim Info data file: " + e.getMessage());
        }
*/	    
	    // ftp the file
	    if (recordsProcessed > 0){
	        if(!this.ftpFile(ftpNamedFile)) {
	            sendAlert = true;
	            alertMessages.add("Failed to FTP data file. MANUAL SEND REQUIRED. File Name: " + ftpNamedFile.getAbsolutePath());
	        }	        
	    }
	    
	    // update state to processed
	    try {
	        if (recordsProcessed > 0) {
	            this.getProcessLog().fine("Changing state of records to READY FOR DELETION, number rows... ");
	            rowsAffected = CustomerInterimInfoBO.changeStateToReadyForDelete();
	            this.getProcessLog().fine("Completed changing state of records to READY FOR DELETION, number rows: " + rowsAffected);
	        }
	    }
	    catch (Exception e) {
	        sendAlert = true;
	        alertMessages.add("Failed to update State of Batch Customer Interim Info records to READY_FOR_DELETION. " + e.getMessage());
	        this.getProcessLog().severe("Failed to update State of Batch Customer Interim Info records to READY_FOR_DELETION. " + e.getMessage());
        }
	    
	    // delete old records
	    if (!this.purgeOldCustInterimRecords()) {
	        sendAlert = true;
	    }
	    
	    // Send any reports
	    this.endTime = Calendar.getInstance();
	    this.sendReport();
	    
	    // Send any alerts
	    if (sendAlert) {
	        this.sendAlert();
	    }
	    this.getProcessLog().exiting(this.getClass().getName(), "doWork");
	    this.getProcessLog().log(Level.CONFIG, "Application terminating Normally.");
	    
	    // close log files
		Handler[] handlers = this.getProcessLog().getHandlers();
		for (int i = 0; i < handlers.length; i++) {
			Handler h = handlers[i];
			this.getProcessLog().removeHandler(h);
			h.close();
		}
	}
	
	/**
	 * 
	 * @param file
	 * @return
	 */
	private boolean ftpFile(File file) {
	    this.getProcessLog().entering(this.getClass().getName(), "ftpFile");
	    boolean ftpSuccess = true;
	    
		com.enterprisedt.net.ftp.FTPClient ftp = null;

		try {
			ftp = new com.enterprisedt.net.ftp.FTPClient(this.getFTPHost());

			com.enterprisedt.net.ftp.FTPMessageCollector listener = new com.enterprisedt.net.ftp.FTPMessageCollector();
			ftp.setMessageListener(listener);       

			ftp.login(this.getFtpUserId(),this.getFtpPwd());
			this.getProcessLog().fine(ftp.getLastValidReply().getReplyText());

			// set up passive ASCII transfers
			ftp.setConnectMode(com.enterprisedt.net.ftp.FTPConnectMode.PASV);
			ftp.setType(com.enterprisedt.net.ftp.FTPTransferType.BINARY);

//			ftp.chdir("incoming");
			this.getProcessLog().fine(ftp.getLastValidReply().getReplyText());
                    
			// if a source file is found then publish it
			if (file.exists()) {
				FileInputStream fis = new FileInputStream(file);
				this.getProcessLog().fine("FTPing file: " + file.getAbsolutePath());
				ftp.put(fis, file.getName()+".txt");
				this.getProcessLog().fine(ftp.getLastValidReply().getReplyText());
			}

			ftp.quit();
			this.getProcessLog().fine(ftp.getLastValidReply().getReplyText());
                                
		}
		catch (Exception e) {
			if (ftp != null) {
				try {
					ftp.quit();
				} catch (Exception exp) {}
			}
			ftpSuccess = false;
			alertMessages.add("ftpFile() - " + e.getMessage());
			this.getProcessLog().severe("ftpFile() - " + e.getMessage());
		}
	    this.getProcessLog().exiting(this.getClass().getName(), "ftpFile", new Boolean(ftpSuccess));
		
		return ftpSuccess;
	}
	
	/**
	 * 
	 *
	 */
	private boolean purgeOldCustInterimRecords() {
	    
	    this.getProcessLog().entering(this.getClass().getName(), "purgeOldCustInterimRecords");
	    boolean deleteSuccess = true;
	    try {
	        int numDeleted = CustomerInterimInfoBO.deleteAttributesOlderThanSpecifiedDays(this.getNumberOfDaysBeforePurge());
	        this.getProcessLog().fine("Number of Customer Interim Info records purged: " + numDeleted);
	        this.getProcessingMessages().add("Number of Customer Interim Info records purged (already processed): " + numDeleted);
	    }
	    catch (UsatException e) {
	        deleteSuccess = false;
	        alertMessages.add("Failed to purge old Customer Interim Info attributes: " + e.getMessage());
	        this.getProcessLog().severe("Failed to purge old Customer Interim Info attributes: " + e.getMessage());
        }
	    this.getProcessLog().exiting(this.getClass().getName(), "purgeOldCustInterimRecords");
	    return deleteSuccess;
	}
	
    /**
     * @return Returns the fTPHost.
     */
    public String getFTPHost() {
        return this.FTPHost;
    }
    /**
     * @param host The fTPHost to set.
     */
    public void setFTPHost(String host) {
        this.FTPHost = host;
    }
    /**
     * @return Returns the ftpPwd.
     */
    public String getFtpPwd() {
        return this.ftpPwd;
    }
    /**
     * @param ftpPwd The ftpPwd to set.
     */
    public void setFtpPwd(String ftpPwd) {
        this.ftpPwd = ftpPwd;
    }
    /**
     * @return Returns the ftpUserId.
     */
    public String getFtpUserId() {
        return this.ftpUserId;
    }
    /**
     * @param ftpUserId The ftpUserId to set.
     */
    public void setFtpUserId(String ftpUserId) {
        this.ftpUserId = ftpUserId;
    }
    
    /**
     * Loads runtime configuration settings
     *
     */
    private void loadProperties() {

        try {
            
            File pFile = new File(this.getPropertyFileName());
            URI uri = pFile.toURI();
            URL url = uri.toURL();
                        
            Properties p = new Properties();
            p.load(url.openStream());
            
            Enumeration<Object> enumer = p.keys();
            while(enumer.hasMoreElements()) {
                String key = (String)enumer.nextElement();
                String value = p.getProperty(key);
                if (key.equalsIgnoreCase("custinterimeftpServer")) {
                    this.setFTPHost(value);
                }
                else if (key.equalsIgnoreCase("custinterimeftpUser")) {
                    this.setFtpUserId(value);
                }
                else if (key.equalsIgnoreCase("custinterimeftpPwd")) {
                    this.setFtpPwd(value);
                }
                else if (key.equalsIgnoreCase("jdbcUserId")) {
                }
                else if (key.equalsIgnoreCase("jdbcPwd")) {
                }
                else if (key.equalsIgnoreCase("custinterimbatch_alertReceviers")) {
                    this.setAlertRecipients(value);
                }
                else if (key.equalsIgnoreCase("custinterimbatch_daysBeforePurge")) {
                    try {
                        int numDays = Integer.valueOf(value).intValue();
                        this.setNumberOfDaysBeforePurge(numDays);
                    }
                    catch (Exception e) {
                        System.out.println("Invalid property file setting for number of days before purge. Leaving default value of 2.");
                    }
                }
                else if (key.equalsIgnoreCase("custinterimbatch_logFileDirectory")) {
                    if (value == null || value.trim().length()==0) {
                        // use default
                        continue;
                    }
                    try {
	                    File f = new File(value);
	                    if (!f.exists()) {
	                        // attempt to create the log file directory
	                        if (f.mkdir()) {
	                            System.out.println("Created new Log File Directory: " + f.getAbsolutePath());
	                            this.setLogFileDirName(value);
	                        }
	                        else {
	                            
	                        }
	                    }
                    }
                    catch (Exception e) {
                        System.out.println("Invalid property file setting for log file directory. Leaving default value of " + this.getLogFileDirName());
                    }
                }
                else if (key.equalsIgnoreCase("custinterimbatch_archiveDirectory")) {
                    if (value == null || value.trim().length()==0) {
                        // use default
                        continue;
                    }
                    try {
	                    File f = new File(value);
	                    if (!f.exists()) {
	                        // attempt to create the log file directory
	                        if (f.mkdir()) {
	                            System.out.println("Created new Archive Directory: " + f.getAbsolutePath());
	                            this.setDataOutputDirName(value);
	                        }
	                        else {
	                            System.out.println("Failed to create archive directory using default: " + this.getDataOutputDirName());
	                        }
	                    }
	                    else {
	                        this.setDataOutputDirName(value);
	                    }
                    }
                    catch (Exception e) {
                        System.out.println("Invalid property file setting for arhive file directory. Leaving default value of " + this.getDataOutputDirName());
                    }
                }
                else if (key.equalsIgnoreCase("custinterimbatch_gpgHomeDir")) {
                    if (value == null || value.trim().length()==0) {
                        // use default
                        continue;
                    }
                    try {
	                    File f = new File(value);
	                    if (!f.exists()) {
                            System.out.println("WARNING: Specified GPG HOMEDIR (custinterimbatch_gpgHomeDir) is not valid: " + value);
	                    }
	                    else {
	                        this.setGpgHomeDir(value);
	                    }
                    }
                    catch (Exception e) {
                        System.out.println("Invalid property file setting for custinterimbatch_gpgHomeDir. Leaving default value of " + this.getGpgHomeDir());
                    }
                }
                else if (key.equalsIgnoreCase("custinterimbatch_gpgKeyAlias")) {
                    this.setGpgKeyAlias(value);
                }
                else if (key.equalsIgnoreCase("custinterimbatch_deletePlainTextFile")) {
                    if (value == null || !value.equalsIgnoreCase("false")) {
                        this.setDeletePlainTextFile(true);
                    }
                    else {
                        this.setDeletePlainTextFile(false);
                    }
                }
                else if (key.equalsIgnoreCase("custinterimbatch_deleteArchiveFile")) {
                    if (value != null && value.equalsIgnoreCase("false")) {
                        this.setDeletePlainTextFile(false);
                    }
                    else {
                        this.setDeletePlainTextFile(true);
                    }
                }
                else if (key.equalsIgnoreCase("custinterimbatch_sendEmailReport")) {
                    if (value != null && value.equalsIgnoreCase("false")) {
                        this.setSendReport(false);
                    }
                    else {
                        this.setSendReport(true);
                    }
                }
                else if (key.equalsIgnoreCase("custinterimbatch_reportRecipients")) {
                    this.setReportRecipients(value);
                }
                else if (key.equalsIgnoreCase("custinterimbatch_logLevel")) {
                    try {
                        this.logLevel = Level.parse(value);
                    }
                    catch (Exception e) {
                        // use default
                        System.out.println("Failed to set logging level, invalid level used. Valid values are found in java.util.logging.Level class. Going with Default Level: " + this.logLevel.getName());
                    }
                }
            } // end while more elements
            
            // initialize UTCommon, no longer needed
//            UTCommon.getInitValues();
            // initialiaze Premibum Attribute ids if necessary
        }
        catch (Exception e) {
            System.out.println("CustInterimInfoBatch::loadProperties(): " + e.getMessage());
            e.printStackTrace();
        }
    }
    /**
     * @return Returns the propertyFileName.
     */
    public String getPropertyFileName() {
        return this.propertyFileName;
    }
    /**
     * @param propertyFileName The propertyFileName to set.
     */
    public void setPropertyFileName(String propertyFileName) throws Exception {
        this.propertyFileName = propertyFileName;
        
        if (this.propertyFileName != null) {
            java.io.File f = new File(this.propertyFileName);
            if (!f.exists() || !f.isFile()) {
                throw new Exception ("Property File does not exist or is not a file: " + propertyFileName);
            }
            this.loadProperties();
        }
    }
    
    /**
     * 
     *
     */
    private void sendReport() {
        try {
            this.getProcessLog().entering(this.getClass().getName(), "sendReport");
            
	        EmailAlert eAlert = new EmailAlert();
	        eAlert.setReceiverList(this.getReportRecipients());
	        eAlert.setSender("custInterimBatchProcessor@usatoday.com");
	        eAlert.setSubject("Extranet Customer Interim Information Processing Report");
	        
	        StringBuffer msg = new StringBuffer();
	        msg.append("Time of Report Generation: " + Calendar.getInstance().getTime().toString()).append("\n\n");
	        
	        msg.append("Customer Interim Info Batch Process started at: ").append(this.startTime.getTime().toString()).append("   ");
	        msg.append("\n\nCustomer Interim Info Batch Process completed at: ").append(this.endTime.getTime().toString()).append("\n\n");
	        
	        msg.append("\n\n").append("Total Attribute Records Processed: ").append(this.getRecordsProcessed()).append("\n");
	        msg.append("Total Unique Orders (should be less than or equal to number of records processed): " + this.getOrdersHash().size()).append("\n");
	        
	        if (this.getProcessingMessages().size() > 0 ){
	            msg.append("\nProcessing Detail Message(s): ").append("\n===================\n");
		        for (Iterator<String> iter = processingMessages.iterator(); iter.hasNext();) {
		            String element = iter.next();
		            msg.append(element).append("\n");
		        }
	        }
	        
	        this.getProcessLog().fine("Report Message: " + msg.toString());
	        
	        eAlert.setBodyText(msg.toString());
	        eAlert.sendAlert();
	        this.getProcessLog().exiting(this.getClass().getName(), "sendReport");
        }
        catch (Exception e) {
            this.getProcessLog().severe("sendReport() - Error sending report! " + e.getMessage());
        }
    }
    
    /**
     * Sends our alerts to alert list
     *
     */
    private void sendAlert() {
        try {
            this.getProcessLog().entering(this.getClass().getName(), "sendAlert");
            
	        EmailAlert eAlert = new EmailAlert();
	        eAlert.setReceiverList(this.getAlertRecipients());
	        eAlert.setSender("custInterimBatchProcessor@usatoday.com");
	        eAlert.setSubject("Alert from Customer Interim Info Batch Processor");
	        
	        StringBuffer msg = new StringBuffer();
	        msg.append("Time of Alert: " + Calendar.getInstance().getTime().toString());
	        msg.append("\n\n").append("Alert Messages:\n").append("==============\n\n");
	        
	        for (Iterator<String> iter = alertMessages.iterator(); iter.hasNext();) {
	            String element = iter.next();
	            msg.append(element).append("\n");	            
	        }
	        
	        this.getProcessLog().finest("Alert Message: " + msg.toString());
	        
	        eAlert.setBodyText(msg.toString());
	        eAlert.sendAlert();
	        
            this.getProcessLog().exiting(this.getClass().getName(), "sendAlert");
        }
        catch (Exception e) {
            System.out.println("sendAlert() - Error sending alert! " + e.getMessage());
        }
        
    }
    
    /**
     * 
     */
    public void initializeLogging(String [] args) {
        try {
            if (this.getLogFileDirName() != null && this.getLogFileDirName().length() > 0) {
                
            }
            else {
                this.setLogFileDirName(".");
            }
            
            this.processLog = Logger.getLogger("com.usatoday.batch.CustInterimInfoBatch");

            this.processLog.setLevel(this.logLevel);
            
            this.processLog.addHandler(new java.util.logging.ConsoleHandler());
            
            Calendar cal = Calendar.getInstance();
	        String time = ZeroFilledNumeric.getValue(4, cal.get(Calendar.YEAR)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.MONTH)+1) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.DAY_OF_MONTH)) + ZeroFilledNumeric.getValue(2, cal.get(Calendar.HOUR_OF_DAY));
	        FileHandler f = new FileHandler(this.getLogFileDirName()+ File.separator + "custinterimbatch_" + time + ".log", true);
	        f.setFormatter(new java.util.logging.SimpleFormatter());
            this.processLog.addHandler(f);
            
            this.processLog.log(Level.CONFIG, "Customer Interim Batch Processor Initializing...");
            if (args != null && args.length > 0) {
                this.processLog.config("Application started with a property file specified. " + this.getPropertyFileName());
            }
            else {
                this.processLog.config("Application started without a property file specified. Using default values. ");
            }

            this.processLog.log(Level.CONFIG, "Log Level: " + this.logLevel.toString());

            this.processLog.fine("EFTP Server: "+ this.getFTPHost());
            this.processLog.fine("EFTP User: " + this.getFtpUserId());
            this.processLog.finest("EFTP Password: " + this.getFtpPwd());
            this.processLog.fine("Alert Receivers: " + this.alertRecipients);
            this.processLog.fine("Days Before Purge: " + this.getNumberOfDaysBeforePurge());
            this.processLog.fine("Log File Directory: " + this.getLogFileDirName());
            this.processLog.fine("Archive Directory: " + this.getDataOutputDirName());
            this.processLog.fine("GNUPG Home Directory: " + this.getGpgHomeDir());
            this.processLog.fine("GNUPG Receiver Alias: " + this.getGpgKeyAlias());
            this.processLog.fine("Delete Archive Plain Text Output File: " + this.isDeletePlainTextFile());
            this.processLog.fine("Delete Encrypted Archive Data File: " + this.isDeleteArchiveFile());
            this.processLog.fine("Send Email Report: " + isSendReport());
            this.processLog.fine("Email Report Recipients: " + this.getReportRecipients());
        }
        catch (Exception e) {
            if (this.processLog == null) {
                this.processLog = Logger.getAnonymousLogger();
                this.processLog.addHandler(new java.util.logging.ConsoleHandler());
            }
            this.processLog.severe("Failed to initialize logging for Customer Interim Info Batch Processor! " + e.getMessage());
        }
    }

}